from django.core.urlresolvers import resolve
from django.test import TestCase
from LesTaches.views import *
from selenium import webdriver
import time


#
# # Create your tests here.
#

class TaskTest(TestCase):
    '''Test unitaire de la page accueil sur la racine du projet'''
    def testlisting(self):
        found = resolve('/LesTaches/listing')
        self.assertEqual(found.func, task_listing)
    def testadd(self):
        found = resolve('/LesTaches/add')
        self.assertEqual(found.func, add)
    def test_lien(self):
        #browser = webdriver.Firefox(executable_path='./geckodriver')
        browser = webdriver.Firefox()
        browser.get('http://localhost:8000/LesTaches/listing')
        assert 'Les Taches' in browser.title
        browser.close()

        browser.get('http://localhost:8000/LesTaches/add')
        assert 'add' in browser.title
        browser.close()


    def test_ajout(self):
        browser = webdriver.Firefox()
        browser.get('http://localhost:8000/LesTaches/listing')
        nb=len(browser.find_elements_by_tag_name('tr'))
        baseurl = "http://localhost:8000/LesTaches/add"
        nom = "test1"
        description = "description1"
        closed=True


        xpaths = { 'champNom' : "//input[@id='id_name']",
                   'champDescription' : "//textarea[@id='id_description']",
                   'champClosed' : "//input[@id='id_closed']",
                   'submitButton' :   "//input[@type='submit']"
                 }

        mydriver = webdriver.Firefox(executable_path='./geckodriver')
        mydriver.get(baseurl)
        mydriver.maximize_window()

        #Clear Username TextBox if already allowed "Remember Me"
        mydriver.find_element_by_xpath(xpaths['champNom']).clear()

        #Write Username in Username TextBox
        mydriver.find_element_by_xpath(xpaths['champNom']).send_keys(nom)

        #Clear Password TextBox if already allowed "Remember Me"
        mydriver.find_element_by_xpath(xpaths['champDescription']).clear()

        # Write Password in password TextBox
        mydriver.find_element_by_xpath(xpaths['champDescription']).send_keys(description)

        #Clear Password TextBox if already allowed "Remember Me"
        mydriver.find_element_by_xpath(xpaths['champClosed']).click()


        #Click Login button
        mydriver.find_element_by_xpath(xpaths['submitButton']).click()

        # print(mydriver.page_source)

        assert nb+1 == len(mydriver.find_elements_by_tag_name('tr'))
        mydriver.close()
