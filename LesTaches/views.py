from django.shortcuts import render,get_object_or_404

from django.forms import ModelForm,Textarea, SelectDateWidget
from LesTaches.models import Task
from django import forms
from django.template import RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect
import datetime
# Create your views here.

def home(request,name=None):
    if name:
        return HttpResponse('bonjour '+name)

    else:
        return HttpResponse('bonjour à tous')




class AddForm(ModelForm):
    def __init__(self,*args,**kwargs):
        super(AddForm,self).__init__(*args,**kwargs)
        self.fields['name'].label = "Nom"
        self.fields['name'].widget.attrs['class'] = 'my_class'
        self.fields['description'].label = "description"
        self.fields['schedule_date'].label = "schedule_date"
        self.fields['due_date'].label = "due_date"
        self.fields['closed'].label="closed"
    class Meta:
        model=Task
        fields=('name','description','schedule_date','due_date','closed',)

        widgets={
            'description': Textarea(attrs={'cols':60, 'rows':10}),
            'schedule_date':SelectDateWidget(),
            'due_date':SelectDateWidget()
        }

def task_listing(request):
    objets=Task.objects.all().order_by('created_date')
    return render(request,'LesTaches/list.html',{'objets':objets})

def add(request):
    form = AddForm()
    con={'form':form,'title':'Les taches'}
    if len(request.POST)>0:
        form = AddForm(request.POST)
        con={'form':form}
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/LesTaches/listing')
    return render(request,"LesTaches/add.html",con)


def add2(request):
	form = AddForm()
	con = {'form': form}
	if request.method == 'GET':
		return render(request,'LesTaches/add.html',con,context_instance=RequestContext(request))
	else:
		form = AddForm(request.POST)
		con = {'form': form}
		if form.is_valid():
			form.save()
			return HttpResponseRedirect("/LesTaches/listing")
	return render(request,'LesTaches/add.html',con)


def change(request,name):
    task=get_object_or_404(Task,id=name)
    form = AddForm(instance=task)
    con={'form':form}
    if len(request.POST)>0:
        form = AddForm(request.POST,instance=task)
        con={'form':form}
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/LesTaches/listing')
    return render(request,"LesTaches/modif.html",con)

def change2(request,name):
    task=get_object_or_404(Task,id=name)
    form = AddForm(instance=task)
    con = {'form': form}
    if request.method == 'GET':
        return render(request,'LesTaches/modif.html',con,context_instance=RequestContext(request))
    else:
        form = AddForm(request.POST,instance=task)
        con = {'form': form}
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/LesTaches/listing")
    return render(request,'LesTaches/modif.html',con)

def delete(request,name):
    task=get_object_or_404(Task,id=name)
    task.delete()
    return HttpResponseRedirect('/LesTaches/listing')
    # form = AddForm(instance=task)
    # con = {'form': form}
    # if request.method == 'GET':
    #     return render(request,'LesTaches/modif.html',con,context_instance=RequestContext(request))
    # else:
    #     form = AddForm(request.POST,instance=task)
    #     con = {'form': form}
    #     if form.is_valid():
    #         form.save()
    #         return HttpResponseRedirect("/LesTaches/listing")
    # return render(request,'LesTaches/modif.html',con)
