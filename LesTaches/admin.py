from django.contrib import admin

# Register your models here.
from LesTaches.models import Task

class TaskAdmin(admin.ModelAdmin):
    list_display=('name','description','created_date','schedule_date','colored_due_date','closed')

admin.site.register(Task,TaskAdmin)
