# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-21 13:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('LesTaches', '0002_auto_20160921_1345'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='schedule_date',
            field=models.DateField(auto_now_add=True),
        ),
    ]
