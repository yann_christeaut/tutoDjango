# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-04 14:44
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('LesTaches', '0004_auto_20160921_1404'),
    ]

    operations = [
        migrations.RenameField(
            model_name='task',
            old_name='created_data',
            new_name='created_date',
        ),
    ]
