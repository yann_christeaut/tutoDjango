# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-21 14:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('LesTaches', '0003_auto_20160921_1353'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='created_data',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='task',
            name='schedule_date',
            field=models.DateField(),
        ),
    ]
