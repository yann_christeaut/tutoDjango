from django.shortcuts import render
from django.forms import ModelForm,Textarea
from django.template import RequestContext
from myform.models import Contact
from django import forms
from django.http import HttpResponse
from django.http import HttpResponseRedirect

# Create your views here.


class ContactForm(ModelForm):
    def __init__(self,*args,**kwargs):
        super(ContactForm,self).__init__(*args,**kwargs)
        self.fields['name'].label = "Nom"
        self.fields['firstname'].label = "prenom"
        self.fields['email'].label="mel"
    class Meta:
        model=Contact
        fields=('name','firstname','email','message')
        widgets={
            'message':Textarea(attrs={'cols':60,'rows':10}),
        }



# class ContactForm2(forms.Form):
#     name = forms.CharField(max_length=200,initial="votre nom",label="nom")
#     firstname = forms.CharField(max_length=200,initial="votre prenom",label="prenom")
#     email = forms.EmailField(max_length=200,label="mail")
#     message = forms.CharField(widget=forms.Textarea(attrs={'cols':60,'rows':10}))

def contact(request):
    form = ContactForm()
    con={'form':form}
    if len(request.POST)>0:
        form = ContactForm(request.POST)
        con={'form':form}
        if form.is_valid():
            cont=form.save(commit=False)
            cont.save()
            return HttpResponseRedirect('/Contacts/success')
    return render(request,"myform/contact.html",con)
    # contact_form2 = ContactForm2()
def contact2(request):
    if request.method=='GET':
        return render(request,'myform/contact.html',con,context_instance=RequestContext(request))
    else:
        form=ContactForm(request.POST)
        con={'form':form}
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/Contacts/success')
        return render(request,'myform/contact.html',con)

def success(request):
    return HttpResponse("<p>Success</p>")
    # return render(request,'myform/contact.html',{'contact_form':contact_form,'contact_form2':contact_form2})
